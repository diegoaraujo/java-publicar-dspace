package br.usp.pesquisa.repositorio.dspace;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import br.usp.pesquisa.repositorio.bean.PropertiesDAO;

public class ClienteDspace {

	private String cookie;
	
	private final String enderecoRestDspace = 
	private final String LOGIN = "/login";
	private final String LOGOUT = "/logout";
	private final String COLLECTIONS = "/collections";
	private final String ITEMS = "/items";
	private final String METADATA = "/metadata";
	private final String BITSTREAMS = "/bitstreams";
	private final String DATA = "/data";
	
	public ClienteDspace() throws Exception {
		this.autenticar();
	}
	
	private String obtemCookieAutenticacao(String resposta) {
		return resposta.substring(0, resposta.indexOf(";"));
	}

	private String enderecoRestDspace(){
		return PropertiesDAO.getInstance().getProp("enderecoRestDspace");
	
	
	private void autenticar() throws Exception {
		CloseableHttpClient clienteHttp = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(PropertiesDAO.getInstance().getProp("enderecoRestDspace") + LOGIN);
		httpPost.setHeader("Accept", "application/json");
		List<NameValuePair> paresChaveValor = new ArrayList<>();
		paresChaveValor.add(new BasicNameValuePair("email", PropertiesDAO.getInstance().getProp("emailUsuarioDspace")));
		paresChaveValor.add(new BasicNameValuePair("password", PropertiesDAO.getInstance().getProp("senhaUsuarioDspace")));
		httpPost.setEntity(new UrlEncodedFormEntity(paresChaveValor));
		HttpResponse resposta = clienteHttp.execute(httpPost);
		if (resposta.getStatusLine().getStatusCode() == 200 && resposta.getHeaders("Set-Cookie") != null && resposta.getHeaders("Set-Cookie").length > 0) {
			cookie = obtemCookieAutenticacao(resposta.getHeaders("Set-Cookie")[0].getValue());
		} else {
			throw new Exception("Erro de autenticação com o servidor Dspace");
		}
		EntityUtils.consume(resposta.getEntity());
		clienteHttp.close();
	}
	
	public void close() throws Exception {
		CloseableHttpClient clienteHttp = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(PropertiesDAO.getInstance().getProp("enderecoRestDspace") + LOGOUT);
		httpGet.addHeader("Cookie", cookie);
		HttpResponse resposta = clienteHttp.execute(httpGet);
		EntityUtils.consume(resposta.getEntity());
		clienteHttp.close();
	}
	
	private String obtemUuidItem(String json) {
		return json.substring(json.indexOf("\"uuid\":\"") + "\"uuid\":\"".length(), json.indexOf("\",\"name\":"));
	}
	
	private String obtemUriItem(String json) {
		return PropertiesDAO.getInstance().getProp("enderecoHandleDspace") + "/" + json.substring(json.indexOf("\"handle\":\"") + "\"handle\":\"".length(), json.indexOf("\",\"type\":"));
	}

	public RespostaDspaceBean inserirItem(String json) throws Exception {
		CloseableHttpClient clienteHttp = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(PropertiesDAO.getInstance().getProp("enderecoRestDspace") + COLLECTIONS + "/" + PropertiesDAO.getInstance().getProp("collectionId") + ITEMS);
		httpPost.setHeader("Accept", "application/json");
	    httpPost.setHeader("Content-type", "application/json; charset=UTF-8");
		httpPost.addHeader("Cookie", cookie);
		httpPost.setEntity(new StringEntity(json, Charset.forName("UTF-8")));
		HttpResponse resposta = clienteHttp.execute(httpPost);
		if (resposta.getStatusLine().getStatusCode() != 200) {
			throw new Exception("Não foi possível inserir item:\nCódigo: " + resposta.getStatusLine().getStatusCode()
					+ "\nRazão: " + resposta.getStatusLine().getReasonPhrase());
		}
		HttpEntity conteudoResposta = resposta.getEntity();
		String jsonDevolvido = IOUtils.toString(conteudoResposta.getContent(), "UTF-8");
		System.out.println(jsonDevolvido);
		RespostaDspaceBean respostaBean = new RespostaDspaceBean();
		respostaBean.setIdfrpoddocie(obtemUuidItem(jsonDevolvido));
		respostaBean.setUriddocie(obtemUriItem(jsonDevolvido));
		EntityUtils.consume(resposta.getEntity());
		clienteHttp.close();
		
		return respostaBean;
	}
	
	public void inserirMetaDadosNoItem(String uuid, String json) throws Exception {
		CloseableHttpClient clienteHttp = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(PropertiesDAO.getInstance().getProp("enderecoRestDspace") + ITEMS + "/" + uuid + METADATA);
		httpPost.setHeader("Accept", "application/json");
	    httpPost.setHeader("Content-type", "application/json; charset=UTF-8");
		httpPost.addHeader("Cookie", cookie);
		httpPost.setEntity(new StringEntity(json, Charset.forName("UTF-8")));
		HttpResponse resposta = clienteHttp.execute(httpPost);
		if (resposta.getStatusLine().getStatusCode() != 200) {
			throw new Exception("Não foi possível inserir metadados no item:\nCódigo: " + resposta.getStatusLine().getStatusCode()
					+ "\nRazão: " + resposta.getStatusLine().getReasonPhrase());
		}
		HttpEntity conteudoResposta = resposta.getEntity();
		String jsonDevolvido = IOUtils.toString(conteudoResposta.getContent(), "UTF-8");
		System.out.println(jsonDevolvido);
		EntityUtils.consume(resposta.getEntity());
		clienteHttp.close();
	}
	
	public String inserirMetadadoDoArquivo(String uuid, String nomeArquivo) throws Exception {
		CloseableHttpClient clienteHttp = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(PropertiesDAO.getInstance().getProp("enderecoRestDspace") + ITEMS + "/" + uuid + BITSTREAMS);
		httpPost.setHeader("Accept", "application/json");
	    httpPost.setHeader("Content-type", "application/json; charset=UTF-8");
		httpPost.addHeader("Cookie", cookie);
		httpPost.setEntity(new StringEntity("{\"name\":\"" + nomeArquivo + "\"}", Charset.forName("UTF-8")));
		HttpResponse resposta = clienteHttp.execute(httpPost);
		if (resposta.getStatusLine().getStatusCode() != 200) {
			throw new Exception("Não foi possível inserir metadados do arquivo:\nCódigo: " + resposta.getStatusLine().getStatusCode()
					+ "\nRazão: " + resposta.getStatusLine().getReasonPhrase());
		}
		HttpEntity conteudoResposta = resposta.getEntity();
		String jsonDevolvido = IOUtils.toString(conteudoResposta.getContent(), "UTF-8");
		System.out.println(jsonDevolvido);
		EntityUtils.consume(resposta.getEntity());
		return jsonDevolvido.substring(9, jsonDevolvido.indexOf("\",\"name\":"));
	}
	
	public void inserirArquivo(String uuidArquivo, byte[] bytesArquivo) throws Exception {
		CloseableHttpClient clienteHttp = HttpClients.createDefault();
		HttpPut httpPut = new HttpPut(PropertiesDAO.getInstance().getProp("enderecoRestDspace") + BITSTREAMS + "/" + uuidArquivo + DATA);
		httpPut.setHeader("Accept", "application/json");
//	    httpPut.setHeader("Content-type", "application/json; charset=UTF-8");
		httpPut.addHeader("Cookie", cookie);
		httpPut.setEntity(new ByteArrayEntity(bytesArquivo/*, ContentType.TEXT_PLAIN*/));
		HttpResponse resposta = clienteHttp.execute(httpPut);
		if (resposta.getStatusLine().getStatusCode() != 200) {
			throw new Exception("Não foi possível inserir dados do arquivo:\nCódigo: " + resposta.getStatusLine().getStatusCode()
					+ "\nRazão: " + resposta.getStatusLine().getReasonPhrase());
		}
		HttpEntity conteudoResposta = resposta.getEntity();
		String jsonDevolvido = IOUtils.toString(conteudoResposta.getContent(), "UTF-8");
		System.out.println(jsonDevolvido);
		EntityUtils.consume(resposta.getEntity());
		clienteHttp.close();
	}
	
	public void atualizarMetadadoDoArquivo(String uuidArquivo, String nomeArquivo) throws Exception {
		CloseableHttpClient clienteHttp = HttpClients.createDefault();
		HttpPut httpPut = new HttpPut(PropertiesDAO.getInstance().getProp("enderecoRestDspace") + BITSTREAMS + "/" + uuidArquivo);
		httpPut.setHeader("Accept", "application/json");
	    httpPut.setHeader("Content-type", "application/json; charset=UTF-8");
		httpPut.addHeader("Cookie", cookie);
		httpPut.setEntity(new StringEntity("{\"name\":\"" + nomeArquivo + "\"}", Charset.forName("UTF-8")));
		HttpResponse resposta = clienteHttp.execute(httpPut);
		if (resposta.getStatusLine().getStatusCode() != 200) {
			throw new Exception("Não foi possível inserir dados do arquivo:\nCódigo: " + resposta.getStatusLine().getStatusCode()
					+ "\nRazão: " + resposta.getStatusLine().getReasonPhrase());
		}
		HttpEntity conteudoResposta = resposta.getEntity();
		String jsonDevolvido = IOUtils.toString(conteudoResposta.getContent(), "UTF-8");
		System.out.println(jsonDevolvido);
		EntityUtils.consume(resposta.getEntity());
		clienteHttp.close();
	}
	
	public static void main(String[] args) {
		try {
			ClienteDspace cliente = new ClienteDspace();
			cliente.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
